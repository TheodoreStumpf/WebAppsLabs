/*
 * Name: Theodore Stumpf
 */
var binarySearch, countTags, extractHashTags, sortItems;

/*
 * BINARY SEARCH
 */
binarySearch = function binarySearch(arr, val) {
   var lo, hi, mid;

   // Set lo and hi to the extremes of the array
   lo = 0;
   hi = arr.length;

   while (lo !== hi) {
      // Calculat the midpoint
      mid = Math.floor((hi - lo) / 2.0) + lo;
      if (arr[mid] === val) {
         // Found
         return true;
      } else if (arr[mid] > val) {
         // Move Down
         hi = mid;
      } else {
         // Move up
         if (lo === mid) {
            lo = mid + 1;
         } else {
            lo = mid;
         }
      }
   }
   // Value not found

   return false;
};

/*
 * COUNTING TAGS
 */
countTags = function countTags(items) {
   var tagCounts, i, j;

   // Create empty object
   tagCounts = {};

   // Loop through array
   for (i = 0; i < items.length; i += 1) {
      // Check if 'tags' exists and if it is an array
      if (items[i].hasOwnProperty('tags') && Array.isArray(items[i].tags)) {
         // For each tag
         for (j = 0; j < items[i].tags.length; j += 1) {
            // Add it to the object
            if (tagCounts.hasOwnProperty(items[i].tags[j])) {
               tagCounts[items[i].tags[j]] += 1;
            } else {
               // Creating it if it does not exist
               tagCounts[items[i].tags[j]] = 1;
            }
         }
      }
   }

   return tagCounts;
};

/*
 * EXTRACT HASHTAGS
 */
extractHashTags = function extractHashTags(str) {
   var reg, arr, i;

   // Create a regular expression matching the format of a hashtag
   reg = /#[a-z]+/igm;
   // Get all matches
   arr = str.match(reg);
   if (arr === null) {
      // Return an empty array if no matches are found
      return [];
   }

   // Filter out duplicates
   arr = arr.filter(function(element, index, self) {
      return index === self.indexOf(element);
   });

   // Remove the '#' from the front of the string
   for (i = 0; i < arr.length; i += 1) {
      arr[i] = arr[i].substr(1);
   }

   return arr;
};


/*
 * SORT ITEMS
 */
sortItems = function sortItems(arr) {
   arr.sort(function compare(a, b) {
      var da, db, pa, pb;

      // Get dates
      da = Infinity; db = Infinity;
      if (a.hasOwnProperty('due')) {
         da = a.due.valueOf();
      }
      if (b.hasOwnProperty('due')) {
         db = b.due.valueOf();
      }
      // Compare dates
      if (da !== db) {
         if (da < db) {
            return -1;
         }

         return 1;
      }

      // Get priority
      pa = 0; pb = 0;
      if (a.hasOwnProperty('priority')) {
         if (a.priority === 'medium') {
            pa = 1;
         } else if (a.priority === 'high') {
            pa = 2;
         }
      }
      if (b.hasOwnProperty('priority')) {
         if (b.priority === 'medium') {
            pb = 1;
         } else if (b.priority === 'high') {
            pb = 2;
         }
      }
      // Compare priority
      if (pa !== pb) {
         if (pa > pb) {
            return -1;
         }

         return 1;
      }

      // Compare items
      if (a.item < b.item) {
         return -1;
      }

      return 1;
   });

   // You are changing the array. No need to return anything.
   return null;
};

/*
 * To allow node.js to run our tests. DO NOT CHANGE!
 */
try {
   module.exports = {
      binarySearch: binarySearch,
      countTags: countTags,
      extractHashTags: extractHashTags,
      sortItems: sortItems
   };
} catch (e) {
   // Keep the linter happy...
}
